package factory;



public class FactoryNormal extends AbstractFactory {

    @Override
    public AbstractRoom crearCuarto(String description) {
        return new RoomNormal(description);
    }

    @Override
    public AbstractSalida crearSalida(AbstractRoom room) {
        return new SalidaNormal(room);
    }
    
}