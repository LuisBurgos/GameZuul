package factory;


import java.util.Set;
import java.util.HashMap;
import java.util.Iterator;

public abstract class AbstractRoom {
    
    String description;
    HashMap exits;
    
    public abstract void setExit(String direction, AbstractSalida neighbor);
    public abstract String getShortDescription();
    public abstract String getLongDescription();
    public abstract String getExitString();
    public abstract AbstractRoom go(String direction);

} 