package factory;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


/**
 *
 * @author luisburgos
 */
public class SalidaNormal extends AbstractSalida {
    
    public SalidaNormal(AbstractRoom room){
        this.room = room;
    }

    @Override
    public AbstractRoom getRoom() {
        return room;
    }
    
}
