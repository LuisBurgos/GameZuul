package factory;



public class FactoryIluminado extends AbstractFactory {

    @Override
    public AbstractRoom crearCuarto(String description) {
        return new RoomIluminado(description);
    }

    @Override
    public AbstractSalida crearSalida(AbstractRoom room) {
        return new SalidaPortal(room);
    }
    
}