package factory;



public abstract class AbstractFactory {
    
    public abstract AbstractRoom crearCuarto(String description);
    public abstract AbstractSalida crearSalida(AbstractRoom room);
    
}