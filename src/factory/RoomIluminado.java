package factory;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


import java.util.HashMap;
import java.util.Iterator;
import java.util.Set;

/**
 *
 * @author luisburgos
 */
public class RoomIluminado extends AbstractRoom {
    
    public RoomIluminado(){}
    
    public RoomIluminado(String description) {
 	this.description = description;
 	exits = new HashMap();
    }
    
    @Override
    public void setExit(String direction, AbstractSalida neighbor) {
        exits.put(direction, neighbor);
    }

    @Override
    public String getShortDescription() {
        return description;
    }

    @Override
    public String getLongDescription() {
        return "You are " + description + ".\n" + getExitString();
    }

    @Override
    public String getExitString() {
        
        String returnString = "Exits:";
 	Set keys = exits.keySet();
 	for(Iterator iter = keys.iterator();
 		iter.hasNext(); )
 		returnString += " " + iter.next();
 	return returnString;
    }

    @Override
    public AbstractRoom go(String direction) {
        return ((AbstractSalida)exits.get(direction)).getRoom();
    }

    
}
