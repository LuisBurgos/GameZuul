package domain;

import factory.AbstractRoom;

/**
 * This class represents players in the game. Each player has a current
 * location.
 *
 * @author Michael Kolling
 * @version 1.0 (December 2002)
 */
public class Player {

    private AbstractRoom currentRoom;

    /**
     * Constructor for objects of class Player
     */
    public Player() {
        currentRoom = null;
    }

    /**
     * Return the current room for this player.
     */
    public AbstractRoom getCurrentRoom() {
        return currentRoom;
    }

    /**
     * Set the current room for this player.
     */
    public void setCurrentRoom(AbstractRoom room) {
        currentRoom = room;
    }

    /**
     * Try to walk in a given direction. If there is a door this will change the
     * player's location.
     */
    public void walk(String direction) {
        AbstractRoom nextRoom = null;

        try {
            nextRoom = currentRoom.go(direction);
        } catch (Exception e) {
            //Do nothing
        }

        if (nextRoom == null) {
            System.out.println("There is no door!");
        } else {
            setCurrentRoom(nextRoom);
            System.out.println(nextRoom.getLongDescription());
        }
    }

}
