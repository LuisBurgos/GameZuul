package domain;



import factory.FactoryIluminado;
import factory.FactoryNormal;
import factory.AbstractFactory;
import factory.AbstractFactory;
import factory.AbstractRoom;
import factory.AbstractSalida;
import factory.FactoryIluminado;
import factory.FactoryNormal;

/**
 * This class is the main class of the "World of Zuul" application.
 * "World of Zuul" is a very simple, text based adventure game. Users
 * can walk around some scenery. That's all. It should really be extended
 * to make it more interesting!
 *
 * To play this game, create an instance of this class and call the "play"
 * method.
 *
 * This main class creates and initialises all the others: it creates all
 * rooms, creates the parser and starts the game.
 *
 * @author Michael Kolling and David J. Barnes
 * @version 1.1 (December 2002)
 */
public class Game {

    public static Game instance;   
    private Parser parser;
    private Player player;

    private Game() {
 	player = new Player();
 	parser = new Parser();
 	createRooms();
    }
    
    public static Game getInstance(){
        if(instance == null){
            instance = new Game();
        }
        return instance;        
    }
 
    private void createRooms(){
        
        AbstractFactory factoryNormal    = new FactoryNormal();
        AbstractFactory factoryIluminado = new FactoryIluminado();
        
        AbstractRoom outside, theatre, pub, lab, office;
 	//Crear Cuartos
 	outside = factoryNormal.crearCuarto("outside the main entrance of the university"); 	
 	pub     = factoryNormal.crearCuarto("in the campus pub");
        office  = factoryNormal.crearCuarto("in the computing admin office");
        theatre = factoryIluminado.crearCuarto("in a lecture theatre");
 	lab     = factoryIluminado.crearCuarto("in the campus pub");
 	        
        AbstractSalida officeWest, theatreWest, labEast, labNorth, 
                       outsideEast, outsideSouth, outsideWest,
                       pubEast, labWest;  
        //Crear Salidas
        outsideEast     = factoryNormal.crearSalida(theatre);
        outsideSouth    = factoryNormal.crearSalida(lab);
        outsideWest     = factoryNormal.crearSalida(pub);
        theatreWest     = factoryIluminado.crearSalida(outside);
        pubEast         = factoryNormal.crearSalida(outside);
        officeWest      = factoryIluminado.crearSalida(lab);   
        labEast         = factoryNormal.crearSalida(office);
        labNorth        = factoryNormal.crearSalida(outside);
      
        //Inicializar Puertas
        outside.setExit("south", outsideSouth);
        outside.setExit("east", outsideEast);
 	outside.setExit("west", theatreWest);
 	theatre.setExit("west", theatreWest);
 	pub.setExit("east", pubEast);
 	lab.setExit("north", labNorth);
 	lab.setExit("east", labEast);
 	office.setExit("west", theatreWest);
        
 	//The player starts the game outside
 	player.setCurrentRoom(outside);        
        
    }
    
    /**
    * Main play routine. Loops until end of play.
    */
    public void play() {
 	printWelcome();
 	// Enter the main command loop. Here we repeatedly read commands and
 	// execute them until the game is over.
 	boolean finished = false;
 	
 	while(! finished) {
 		Command command = parser.getCommand();
 		if(command == null) {
 			System.out.println("I don't understand...");
 		} else {
 			finished = command.execute(player);

 		}
 	}
 	System.out.println("Thank you for playing. Good bye.");
    }
    
    /**
    * Print out the opening message for the player.
    */
    private void printWelcome() {
 	System.out.println();
 	System.out.println("Welcome to The World of Zuul!");
 	System.out.println("The World of Zuul is a new, incredibly boring adventure game.");
 	System.out.println("Type 'help' if you need help.");
 	System.out.println();
 	System.out.println(player.getCurrentRoom().getLongDescription());
    }
    
} 